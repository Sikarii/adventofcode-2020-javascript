const fs = require("fs");
const path = require("path");

module.exports = {
  isInRange: (value, low, high) => {
    const num = parseInt(value);
    return !isNaN(num) && num >= low && num <= high;
  },

  sumNumbers(numbers, initial = 0) {
    return numbers.reduce((total, num) => {
      return total + num;
    }, initial);
  },

  productOfNumbers(numbers) {
    return numbers.reduce((total, num) => {
      return total * num;
    }, 1);
  },

  *generateRange(start, end, step = 1) {
    for (let i = start; i <= end; i += step) {
      yield i;
    }
  },

  getInputLines: (dirName, separator = "\r\n") => {
    const inputPath = path.join(dirName, "input.txt");
    const inputString = fs.readFileSync(inputPath, "utf-8");
    const inputLines = inputString.trim().split(separator);
    return inputLines;
  },
};
