const { getInputLines } = require("../helpers");

const numbers = getInputLines(__dirname).map(Number);

module.exports = {
  inputs: {
    part1: [...numbers],
    part2: [...numbers],
  },

  expectedOutputs: {
    part1: 788739,
    part2: 178724430,
  },
};
