module.exports = numbers => {
  for (const num1 of numbers) {
    for (const num2 of numbers) {
      const partialSum = num1 + num2;
      if (partialSum > 2020) {
        continue;
      }

      for (const num3 of numbers) {
        const sum = partialSum + num3;
        if (sum === 2020) {
          return num1 * num2 * num3;
        }
      }
    }
  }
};
