module.exports = numbers => {
  for (const num1 of numbers) {
    const num2 = numbers.find(n => n + num1 === 2020);
    if (num2 === undefined) {
      continue;
    }

    return num1 * num2;
  }
};
