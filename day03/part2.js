module.exports = map => {
  const slopes = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2],
  ];

  let result = 1;

  for (const slope of slopes) {
    const [slopeX, slopeY] = slope;

    let slopeTrees = 0;
    let [x, y] = [0, 0];

    while (y < map.length) {
      const current = map[y][x % map[y].length];
      if (current === "#") {
        slopeTrees++;
      }

      x += slopeX;
      y += slopeY;
    }

    result *= slopeTrees;
  }

  // TODO: I think most of functionality can be shared with part1?
  return result;
};
