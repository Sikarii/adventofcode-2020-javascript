module.exports = map => {
  let trees = 0;
  let [x, y] = [0, 0];

  while (y < map.length) {
    const current = map[y][x % map[y].length];
    if (current === "#") {
      trees++;
    }

    x += 3;
    y += 1;
  }

  return trees;
};
