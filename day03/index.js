const { getInputLines } = require("../helpers");

const map = getInputLines(__dirname).map(l => [...l]);

module.exports = {
  inputs: {
    part1: [...map],
    part2: [...map],
  },

  expectedOutputs: {
    part1: 184,
    part2: 2431272960,
  },
};
