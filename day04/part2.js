const { isInRange } = require("../helpers");

const hasNumDigits = (input, numDigits) => {
  const value = parseInt(input);
  return !isNaN(value) && input.length === numDigits;
};

module.exports = passports => {
  const validators = {
    byr: input => {
      return hasNumDigits(input, 4) && isInRange(input, 1920, 2002);
    },

    iyr: input => {
      return hasNumDigits(input, 4) && isInRange(input, 2010, 2020);
    },

    eyr: input => {
      return hasNumDigits(input, 4) && isInRange(input, 2020, 2030);
    },

    hgt: input => {
      const system = input.slice(-2);
      const value = input.slice(0, -2);

      if (system === "cm") {
        return isInRange(value, 150, 193);
      } else if (system === "in") {
        return isInRange(value, 59, 76);
      }

      return false;
    },

    hcl: input => {
      const [symbol, ...rest] = input;

      return (
        symbol === "#" && !rest.some(c => "0123456789abcdef".indexOf(c) === -1)
      );
    },

    ecl: input => {
      return ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].includes(input);
    },

    pid: input => {
      return hasNumDigits(input, 9);
    },
  };

  const required = Object.keys(validators);

  const valids = passports.filter(p => {
    return required.every(f => {
      const exists = p[f] !== undefined;
      return exists && validators[f](p[f]);
    });
  });

  return valids.length;
};
