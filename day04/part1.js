module.exports = passports => {
  const required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

  const valids = passports.filter(p => {
    return required.every(f => p[f] !== undefined);
  });

  return valids.length;
};
