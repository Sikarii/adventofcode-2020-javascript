const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname, "\r\n\r\n");

const passports = lines.map(r => {
  const rawPassport = r.split("\r\n").join(" ");

  const passport = {};
  const entries = rawPassport.split(" ");

  for (const entry of entries) {
    const [key, value] = entry.split(":");
    passport[key] = value;
  }

  return passport;
});

module.exports = {
  inputs: {
    part1: [...passports],
    part2: [...passports],
  },

  expectedOutputs: {
    part1: 219,
    part2: 127,
  },
};
