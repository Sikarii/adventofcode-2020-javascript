const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);

const instructions = [];

lines.forEach(line => {
  const [lhs, rhs] = line.split(" = ");

  if (lhs === "mask") {
    return instructions.push({
      mask: rhs,
      memsets: [],
    });
  }

  // Cut "mem[" and "]"
  const address = lhs.slice(4, -1);
  const currIdx = instructions.length - 1;

  instructions[currIdx].memsets.push({
    address: parseInt(address),
    value: parseInt(rhs),
  });
});

module.exports = {
  inputs: {
    part1: [...instructions],
    part2: [...instructions],
  },

  expectedOutputs: {
    part1: 9879607673316,
    part2: 3435342392262,
  },

  shared: {},
};
