const { sumNumbers } = require("../helpers");

module.exports = instructions => {
  const memory = {};

  for (const instruction of instructions) {
    for (const memset of instruction.memsets) {
      const value = memset.value.toString(2).padStart(36, "0");
      const newValue = applyMask(value, instruction.mask);

      memory[memset.address] = newValue;
    }
  }

  const sum = sumNumbers(Object.values(memory));
  return sum;
};

const applyMask = (value, mask) => {
  const result = [...value];

  for (let i = 0; i < mask.length; i++) {
    const maskChar = mask[mask.length - i - 1];
    if (maskChar !== "X") {
      result[result.length - i - 1] = maskChar;
    }
  }

  const binaryStr = result.join("");
  const binaryValue = parseInt(binaryStr, 2);

  return binaryValue;
};
