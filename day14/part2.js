const { sumNumbers } = require("../helpers");

module.exports = instructions => {
  const memory = {};

  for (const instruction of instructions) {
    for (const memset of instruction.memsets) {
      const addr = memset.address.toString(2).padStart(36, "0");
      const permutations = getPermutations(addr, instruction.mask);

      permutations.forEach(permutation => {
        memory[permutation] = memset.value;
      });
    }
  }

  const sum = sumNumbers(Object.values(memory));
  return sum;
};

const getPermutations = (addr, mask) => {
  const result = [[...addr]];

  for (let i = 0; i < addr.length; i++) {
    const maskChar = mask[mask.length - i - 1];

    if (maskChar === "1") {
      result.forEach(r => {
        r[r.length - i - 1] = maskChar;
      });
    }

    if (maskChar === "X") {
      result.forEach(r => {
        r[r.length - i - 1] = "0";
      });

      const temp = result.map(r => [...r]);

      result.forEach(r => {
        r[r.length - i - 1] = "1";
      });

      result.push(...temp);
    }
  }

  const permutations = result.map(p => {
    const binaryStr = p.join("");
    const binaryValue = parseInt(binaryStr, 2);
    return binaryValue;
  });

  return permutations;
};
