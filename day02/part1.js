module.exports = candidates => {
  const valids = candidates.filter(c => {
    const charInPass = [...c.password].filter(x => x === c.character).length;
    return charInPass >= c.lowerRange && charInPass <= c.upperRange;
  });

  return valids.length;
};
