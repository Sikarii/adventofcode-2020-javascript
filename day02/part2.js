module.exports = candidates => {
  const valids = candidates.filter(c => {
    const {
      password: pass,
      character: char,
      position1: pos1,
      position2: pos2,
    } = c;

    return (
      (pass.charAt(pos1 - 1) === char && pass.charAt(pos2 - 1) !== char) ||
      (pass.charAt(pos2 - 1) === char && pass.charAt(pos1 - 1) !== char)
    );
  });

  return valids.length;
};
