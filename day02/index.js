const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);

const candidates = lines.map(l => {
  const [range, char, pass] = l.split(" ");
  const [lower, upper] = range.split("-");

  return {
    password: pass,
    character: char.charAt(0),
    lowerRange: parseInt(lower),
    upperRange: parseInt(upper),
  };
});

module.exports = {
  inputs: {
    part1: [...candidates],
    part2: [
      ...candidates.map(c => {
        const { lowerRange: position1, upperRange: position2, ...rest } = c;

        return {
          ...rest,
          position1,
          position2,
        };
      }),
    ],
  },

  expectedOutputs: {
    part1: 607,
    part2: 321,
  },
};
