const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);

const estimate = parseInt(lines[0]);
const busses = lines[1].split(",").map(Number);

const serving = busses.filter(b => !isNaN(b));

const input1 = {
  estimate: estimate,
  busses: [...serving],
};

const input2 = busses
  .map((bus, index) => {
    return {
      id: bus,
      index: index,
    };
  })
  .filter(b => !isNaN(b.id));

module.exports = {
  inputs: {
    part1: input1,
    part2: [...input2],
  },

  expectedOutputs: {
    part1: 2382,
    part2: 906332393333683,
  },

  shared: {},
};
