const { productOfNumbers } = require("../helpers");

module.exports = busses => {
  const totalProduct = productOfNumbers(busses.map(b => b.id));

  let sum = 0;
  for (const bus of busses) {
    const partProduct = totalProduct / bus.id;

    for (let i = 0; ; i++) {
      const test = i * partProduct;
      if (test % bus.id === bus.index % bus.id) {
        sum += test;
        break;
      }
    }
  }

  return Math.abs((sum % totalProduct) - totalProduct);
};
