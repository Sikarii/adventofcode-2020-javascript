module.exports = input => {
  let lowestId = 0;
  let lowest = input.estimate + input.busses[0];

  input.busses.forEach(bus => {
    const divided = input.estimate / bus;
    const next = Math.ceil(divided) * bus;

    if (next < lowest) {
      lowest = next;
      lowestId = bus;
    }
  });

  const mins = lowest - input.estimate;
  return lowestId * mins;
};
