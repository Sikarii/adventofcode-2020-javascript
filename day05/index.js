const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);
const passes = lines.map(l => {
  const row = l.slice(0, 7);
  const col = l.slice(-3);

  // prettier-ignore
  const rowBinary = row
    .replace(/[F+]/g, "0")
    .replace(/[B+]/g, "1");

  // prettier-ignore
  const colBinary = col
    .replace(/[L+]/g, "0")
    .replace(/[R+]/g, "1");

  const rowNum = parseInt(rowBinary, 2);
  const colNum = parseInt(colBinary, 2);
  const seatId = rowNum * 8 + colNum;

  return {
    row: rowNum,
    col: colNum,
    seat: seatId,
  };
});

module.exports = {
  inputs: {
    part1: [...passes],
    part2: [...passes],
  },

  expectedOutputs: {
    part1: 911,
    part2: 629,
  },
};
