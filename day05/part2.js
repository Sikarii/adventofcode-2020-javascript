const { generateRange } = require("../helpers");

module.exports = passes => {
  // prettier-ignore
  const sortedSeatIds = passes
    .map(p => p.seat)
    .sort((a, b) => a - b);

  const min = sortedSeatIds[0];
  const [max] = sortedSeatIds.slice(-1);
  const range = [...generateRange(min, max)];

  const missingSeatId = range.find(id => {
    return !sortedSeatIds.includes(id);
  });

  return missingSeatId;
};
