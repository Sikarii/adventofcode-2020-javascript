module.exports = passes => {
  const maxSeatId = Math.max(...passes.map(p => p.seat));
  return maxSeatId;
};
