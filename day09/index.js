const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);
const numbers = lines.map(Number);

module.exports = {
  inputs: {
    part1: [...numbers],
    part2: [...numbers],
  },

  expectedOutputs: {
    part1: 32321523,
    part2: 4794981,
  },
};
