module.exports = numbers => {
  for (let i = 0; i < numbers.length; i++) {
    let x = 0;
    let total = 0;

    for (x = i; x < numbers.length; x++) {
      total += numbers[x];

      if (total >= 32321523) {
        break;
      }
    }

    if (total === 32321523) {
      const nums = numbers.slice(i, x + 1);
      const sorted = nums.sort();

      const min = sorted[0];
      const max = sorted.slice(-1)[0];

      return min + max;
    }
  }
};
