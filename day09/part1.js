module.exports = numbers => {
  for (let i = 25; i < numbers.length; i++) {
    const previous = numbers.slice(i - 25, i);

    if (!solveProblem(numbers[i], previous)) {
      return numbers[i];
    }
  }
};

const solveProblem = (total, nums) => {
  if (nums.length <= 1) {
    return false;
  }

  for (const num1 of nums) {
    for (const num2 of nums) {
      const sum = num1 + num2;
      if (total === sum) {
        return true;
      }
    }
  }

  return false;
};
