// ✅
// ⚠️
// ❌

module.exports.red = msg => {
  return `\x1b[31m${msg}\x1b[0m`;
};

module.exports.cyan = msg => {
  return `\x1b[36m${msg}\x1b[0m`;
};

module.exports.orange = msg => {
  return `\x1b[33m${msg}\x1b[0m`;
};

module.exports.prompt = `\n${this.cyan("aoc-cli")}> `;
