const fs = require("fs");
const path = require("path");

// prettier-ignore
const {
  getDayPath,
  getDayPartPath,
  uncachedRequire
} = require("./utils");

const requireUncached = uncachedRequire(require);

const getPartDataProp = (part, data) => {
  const [parent, _child] = part.split("-");

  const partKey = `part${part}`;
  const parentKey = `part${parent}`;

  return data?.[partKey] ?? data?.[parentKey];
};

module.exports.runDayParts = (day, parts) => {
  const results = {};
  const partsData = this.getDayPartsData(day, parts);

  for (const part of parts) {
    const data = partsData[part];

    const run = this.resolveDayPart(day, part);
    const result = run(data.input, data.shared);

    results[part] = {
      data: data,
      output: result,
    };
  }

  return results;
};

module.exports.getDayParts = day => {
  const dayPath = getDayPath(day);
  const dirPath = path.resolve(__dirname, dayPath);

  const files = fs
    .readdirSync(dirPath)
    .filter(f => f.startsWith("part") && f.endsWith(".js"));

  const names = files.map(f => {
    const name = path.basename(f, ".js");
    const numOnly = name.substring(4);
    return numOnly;
  });

  const sortedNames = names.sort();
  return sortedNames;
};

module.exports.getDayPartsData = (day, parts) => {
  const dayPath = getDayPath(day);
  const dayData = requireUncached(dayPath);

  const partsData = {};

  for (const part of parts) {
    const { inputs, expectedOutputs } = dayData;

    const input = getPartDataProp(part, inputs);
    const expected = getPartDataProp(part, expectedOutputs);

    partsData[part] = {
      input: input,
      shared: dayData.shared,
      expectedOutput: expected,
    };
  }

  return partsData;
};

module.exports.resolveDayPart = (day, part) => {
  const partPath = getDayPartPath(day, part);
  const resolvedFunc = requireUncached(partPath);

  if (typeof resolvedFunc !== "function") {
    return function () {};
  }

  return resolvedFunc;
};
