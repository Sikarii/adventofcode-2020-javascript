const fs = require("fs");
const readline = require("readline");

const { prompt } = require("./constants");
const { clearConsole } = require("./utils");

const state = {
  watchers: [],
};

const commandDefaults = {
  aliases: [],
  description: "No description",
};

const commands = {};

const commandFiles = fs
  .readdirSync("./cli/commands")
  .filter(f => f.endsWith(".js"));

for (const file of commandFiles) {
  const rawCommand = require(`./commands/${file}`);

  const command = {
    ...commandDefaults,
    ...rawCommand,
  };

  commands[command.name] = command;

  for (const alias of command.aliases) {
    commands[alias] = {
      ...command,
      isAlias: true,
    };
  }
}

const executeCommand = async (commandName, args) => {
  const command = commands[commandName];
  await command?.execute({
    rl,
    args,
    state,
    commands,
  });
};

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: prompt,
  completer: line => {
    const completions = Object.keys(commands);
    const hits = completions.filter(c => c.startsWith(line));
    return [hits, line];
  },
});

rl.on("line", async line => {
  const [commandName, ...args] = line.split(" ");

  try {
    await executeCommand(commandName, args);
  } catch (e) {
    console.log(`\x1b[31m${e.stack}\x1b[0m`);
  }

  return rl.prompt();
});

clearConsole(rl);
rl.prompt(false);
