const fs = require("fs");
const path = require("path");

module.exports.clearConsole = rl => {
  rl.write(null, {
    ctrl: true,
    name: "l",
  });
};

module.exports.getDayPath = day => {
  const paddedDay = day.padStart(2, "0");
  return `../day${paddedDay}`;
};

module.exports.getDayPartPath = (day, part) => {
  const dayPath = this.getDayPath(day);
  return `${dayPath}/part${part}`;
};

module.exports.assertDayExists = day => {
  const dayPath = this.getDayPath(day);
  const dayFile = path.resolve(__dirname, dayPath, "index.js");

  if (!fs.existsSync(dayFile)) {
    throw new Error(`Day ${day} does not exist!`);
  }
};

module.exports.assertPartExists = (day, part) => {
  const partPath = this.getDayPartPath(day, part);
  const partFile = path.resolve(__dirname, partPath) + ".js";

  if (!fs.existsSync(partFile)) {
    throw new Error(`Part ${part} for day ${day} does not exist!`);
  }
};

// https://stackoverflow.com/a/16060619
module.exports.uncachedRequire = require => module => {
  delete require.cache[require.resolve(module)];
  return require(module);
};
