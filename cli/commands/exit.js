const { clearConsole } = require("../utils");

module.exports = {
  name: "exit",
  description: "Exits watch mode",
  execute: ({ rl, state }) => {
    state.watchers.forEach(w => w?.close());
    state.watchers.splice(0, state.watchers.length);
    clearConsole(rl);
  },
};
