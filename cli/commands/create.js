const fs = require("fs");
const path = require("path");

const { cyan, orange } = require("../constants");

module.exports = {
  name: "create",
  aliases: ["make"],
  description: "Creates a new day directory structure",
  execute: ({ args }) => {
    const day = args[0];
    const paddedDay = day.padEnd(2, "0");

    const dayPath = path.join(__dirname, "..", "..", `day${paddedDay}`);

    if (fs.existsSync(dayPath)) {
      throw new Error(dayExistsMessage);
    }

    fs.mkdirSync(dayPath);

    const templatesPath = path.join(__dirname, "..", "..", "template");
    const templateFiles = fs.readdirSync(templatesPath);

    for (const file of templateFiles) {
      const destFile = path.join(dayPath, file);
      const templateFile = path.join(templatesPath, file);

      fs.copyFileSync(templateFile, destFile);
    }

    console.log(createMessage(day));
  },
};

const createMessage = day =>
  `Created ${cyan("files")} for ${orange(`day ${day}`)}`;

const dayExistsMessage = `Directory for the day already exists, refusing to overwrite!`;
