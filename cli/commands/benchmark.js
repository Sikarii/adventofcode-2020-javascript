const { performance } = require("perf_hooks");

const { sumNumbers } = require("../../helpers");
const { cyan, orange } = require("../constants");

const {
  getDayParts,
  getDayPartsData,
  resolveDayPart,
} = require("../interface");

module.exports = {
  name: "benchmark",
  aliases: ["bench"],
  description: "Benchmarks given day (and parts)",
  execute: ({ args }) => {
    const [day, partStr] = args[0].split(":");
    const iterationCount = parseInt(args[1]) || 1000;

    const parts = partStr?.split(",") ?? [];

    if (!parts || !parts.length) {
      const allParts = getDayParts(day);
      parts.push(...allParts);
    }

    const setups = [];
    const partsData = getDayPartsData(day, parts);

    for (const part of parts) {
      const data = partsData[part];

      const run = resolveDayPart(day, part);
      const benchmarkFunc = run.bind(null, data.input, data.shared);

      setups.push({
        part,
        func: benchmarkFunc,
      });
    }

    const maxName = Math.max(...parts.map(p => p.length));

    for (const setup of setups) {
      const partPadded = setup.part.padEnd(maxName);
      const avgNs = runIterations(setup.func, iterationCount);

      console.log(
        resultMessage({
          part: partPadded,
          avgNs: avgNs,
          iterations: iterationCount,
        })
      );
    }
  },
};

const runIterations = (func, iterations) => {
  const durations = [];

  for (let i = 0; i < iterations; i++) {
    const start = performance.now();

    func();

    const end = performance.now();
    durations.push(end - start);
  }

  const sum = sumNumbers(durations);
  const avgMs = sum / durations.length;
  const avgNs = 1000000 * avgMs;

  return avgNs;
};

const resultMessage = ({ part, avgNs, iterations }) => {
  const x = orange(`Part ${part}`);
  const y = cyan("nanoseconds / call") + " over";
  const z = orange(iterations) + " iterations";
  const i = cyan(avgNs.toFixed(6));

  return `[${x}] Average ${y} ${z}: ${i}`;
};
