const { cyan, orange } = require("../constants");

module.exports = {
  name: "help",
  aliases: ["h"],
  description: "Lists all commands",
  execute: ({ commands }) => {
    const cmds = Object.values(commands)
      .filter(c => !c.isAlias)
      .sort(c => c.name);

    console.log(headerMessage);

    for (const cmd of cmds) {
      const name = [...cmd.aliases, cmd.name].join(", ");

      console.log(
        commandHelpMessage({
          name: name,
          description: cmd.description,
        })
      );
    }
  },
};

const headerMessage = `Available commands:`;

const commandHelpMessage = ({ name, description }) =>
  `- ${cyan(name)} - ${orange(description)}`;
