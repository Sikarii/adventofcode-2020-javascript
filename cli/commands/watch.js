const fs = require("fs");
const { once } = require("events");

const { runDayParts, getDayParts } = require("../interface");
const { clearConsole, assertDayExists } = require("../utils");

const { red, cyan, orange, prompt } = require("../constants");

module.exports = {
  name: "watch",
  aliases: ["w"],
  description: "Watches a day and executes it on change",
  execute: async ({ args, rl, state }) => {
    const day = args[0];
    assertDayExists(day);

    rl.setPrompt("");

    const paddedDay = day.padStart(2, "0");

    const watcher = fs.watch(`./day${paddedDay}/`, () => {
      clearConsole(rl);
      console.log(headerMessage(day));

      const parts = getDayParts(day);
      const results = runDayParts(day, parts);
      const maxName = Math.max(...parts.map(p => p.length));

      for (const part of parts) {
        const result = results[part];

        const messageData = {
          part: part.padEnd(maxName),
          output: result.output,
          expected: result.data.expectedOutput,
        };

        const message =
          result.output === result.data.expectedOutput
            ? successMessage(messageData)
            : failureMessage(messageData);

        console.log(message);
      }
    });

    watcher.emit("change");
    state.watchers.push(watcher);

    await once(watcher, "close");
    rl.setPrompt(prompt);
  },
};

const headerMessage = day =>
  `[${cyan(`WATCHING DAY ${day}`)}]\n"exit" to return`;

const successMessage = ({ part, output }) =>
  `[${orange(`Part ${part}`)}] ✅ ${cyan(output)}`;

const failureMessage = ({ part, output, expected }) =>
  `[${orange(`Part ${part}`)}] ❌ ${red(output)} != ${cyan(expected)}`;
