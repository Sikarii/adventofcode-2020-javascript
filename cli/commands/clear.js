const { clearConsole } = require("../utils");

module.exports = {
  name: "clear",
  description: "Clears the console",
  execute: ({ rl }) => clearConsole(rl),
};
