const { red, cyan, orange } = require("../constants");
const { runDayParts, getDayParts } = require("../interface");
const { assertDayExists, assertPartExists } = require("../utils");

module.exports = {
  name: "run",
  description: "Executes a day (and its parts)",
  execute: async ({ args }) => {
    const dayWithParts = args[0].split(":");

    const day = dayWithParts[0];
    const parts = dayWithParts[1]?.split(",") ?? [];

    assertDayExists(day);
    parts.forEach(p => assertPartExists(day, p));

    if (!parts || !parts.length) {
      const allParts = getDayParts(day);
      parts.push(...allParts);
    }

    const results = runDayParts(day, parts);
    const maxName = Math.max(...parts.map(p => p.length));

    for (const part of parts) {
      const result = results[part];

      const messageData = {
        part: part.padEnd(maxName),
        output: result.output,
        expected: result.data.expectedOutput,
      };

      const message =
        result.output === result.data.expectedOutput
          ? successMessage(messageData)
          : failureMessage(messageData);

      console.log(message);
    }
  },
};

const successMessage = ({ part, output }) =>
  `[${orange(`Part ${part}`)}] ✅ ${cyan(output)}`;

const failureMessage = ({ part, output, expected }) =>
  `[${orange(`Part ${part}`)}] ❌ ${red(output)} != ${cyan(expected)}`;
