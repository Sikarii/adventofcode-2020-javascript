module.exports = {
  name: "quit",
  description: "Quits the app",
  execute: () => process.exit(0),
};
