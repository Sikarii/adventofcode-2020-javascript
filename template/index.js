const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);

module.exports = {
  inputs: {
    part1: [...lines],
    part2: [...lines],
  },

  expectedOutputs: {
    part1: undefined,
    part2: undefined,
  },

  shared: {},
};
