module.exports = instructions => {
  let cursor = 0;
  let accumulator = 0;

  const actions = {
    acc: amt => {
      accumulator += amt;
      cursor += 1;
    },

    jmp: amt => {
      cursor += amt;
    },

    nop: () => cursor++,
  };

  while (cursor < instructions.length) {
    const instruction = instructions[cursor];
    if (instruction.visited) {
      break;
    }

    instruction.visited = true;
    actions[instruction.operation](instruction.amount);
  }

  return accumulator;
};
