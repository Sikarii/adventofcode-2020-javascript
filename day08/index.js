const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);

const instructions = lines.map((l, index) => {
  const [operation, amount] = l.split(" ");

  return {
    index,
    operation,
    amount: parseInt(amount),
    visited: false,
  };
});

module.exports = {
  inputs: {
    part1: [...instructions],
    part2: [...instructions],
  },

  expectedOutputs: {
    part1: 1928,
    part2: 1319,
  },
};
