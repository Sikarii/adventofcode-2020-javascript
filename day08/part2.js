module.exports = instructions => {
  let cursor = 0;
  let accumulator = 0;

  const actions = {
    acc: amt => {
      accumulator += amt;
      cursor += 1;
    },

    jmp: amt => {
      cursor += amt;
    },

    nop: () => cursor++,
  };

  const jmpNopInstructions = instructions.filter(
    i => i.operation === "jmp" || (i.operation === "nop" && i.amount !== 0)
  );

  for (const jmpNopInstr of jmpNopInstructions) {
    cursor = 0;
    accumulator = 0;

    instructions.forEach(instruction => {
      instruction.visited = false;
    });

    while (cursor < instructions.length) {
      const instr = instructions[cursor];
      if (instr.visited) {
        break;
      }

      if (instr.index === jmpNopInstr.index) {
        if (instr.operation === "nop") {
          actions.jmp(instr.amount);
        } else {
          actions.nop(instr.amount);
        }

        continue;
      }

      actions[instr.operation](instr.amount);
      instr.visited = true;
    }

    if (cursor >= instructions.length) {
      return accumulator;
    }
  }
};
