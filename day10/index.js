const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);
const rawJolts = lines.map(Number).sort((a, b) => a - b);

const jolts = [0, ...rawJolts, Math.max(...rawJolts) + 3];

module.exports = {
  inputs: {
    part1: [...jolts],
    part2: [...jolts],
  },

  expectedOutputs: {
    part1: 2482,
    part2: 96717311574016,
  },
};
