const { sumNumbers } = require("../helpers");

module.exports = jolts => {
  const paths = {};
  const reversedJolts = [...jolts].reverse();

  reversedJolts.forEach(jolt => {
    const jumps = jolts.filter(jump => {
      return jump > jolt && jump <= jolt + 3;
    });

    const mapped = jumps.map(jump => paths[jump]);
    const jumpsSum = sumNumbers(mapped);

    paths[jolt] = jumpsSum || 1;
  });

  return paths[0];
};
