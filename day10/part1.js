module.exports = jolts => {
  const diffs = jolts.map((value, idx) => {
    return jolts[idx + 1] - value;
  });

  const diffs1 = diffs.filter(d => d === 1);
  const diffs3 = diffs.filter(d => d === 3);

  return diffs1.length * diffs3.length;
};
