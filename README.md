## My attempt at [Advent of Code](https://adventofcode.com/) 2020 with Javascript (NodeJS).

---

- NodeJS v14
- No external dependencies

---

### Running the puzzles

This project has a command-line interface which you can use to interact with the puzzles.

The CLI can simply be started by running `node cli`.

#### Available commands:

- `bench`, `benchmark` - Benchmarks given day (and parts)
- `clear` - Clears the console
- `make`, `create` - Creates a new day directory structure
- `exit` - Exits watch mode
- `h`, `help` - Lists all commands
- `quit` - Quits the app
- `run` - Executes a day (and its parts)
- `w`, `watch` - Watches a day and executes it on change

#### A few examples:

- `run 1` - Run all parts of day 1.
- `run 1:2` - Run only part 2 of day 1.
- `run 1:1,2` - Run part 1 and part 2 of day.
- `watch 5` - Watch all parts of day 5 for changes and runs them on change.
- `make 10` - Creates a new directory structure (and setup) for day 10.
- `benchmark 5:2 1000` - Benchmark part 2 of day 5 for 1000 iterations.

---
