module.exports = bagMap => {
  const recurseBagCount = (map, bag) => {
    const contents = map.get(bag);
    if (contents === null) {
      return 0;
    }

    return contents.reduce((acc, r) => {
      return acc + r.count * (1 + recurseBagCount(map, r.color));
    }, 0);
  };

  const total = recurseBagCount(bagMap, "shiny gold");
  return total;
};
