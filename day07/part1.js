module.exports = bagMap => {
  const recurseBag = (myBag, map, bag) => {
    if (bag === myBag) {
      return true;
    }

    const contents = map.get(bag);
    if (contents === null) {
      return false;
    }

    return contents.reduce((a, r) => {
      return a || recurseBag(myBag, map, r.color);
    }, false);
  };

  const myBag = "shiny gold";
  const counts = [];

  for (const [inner] of bagMap) {
    if (inner !== myBag) {
      const count = recurseBag(myBag, bagMap, inner);
      counts.push(count);
    }
  }

  const sum = counts.reduce((a, b) => a + b, 0);
  return sum;
};
