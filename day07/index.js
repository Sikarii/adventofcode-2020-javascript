const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);

const bagMap = lines.reduce((map, line) => {
  const [color, rawInner] = line.split(" bags contain ");
  const innerString = rawInner.slice(0, -1);

  if (innerString === "no other bags") {
    return map.set(color, null);
  }

  const inner = innerString.split(", ").map(c => {
    const [count, ...rest] = c.split(" ");
    const innerColor = rest.slice(0, 2).join(" ");

    return {
      color: innerColor,
      count: parseInt(count),
    };
  });

  return map.set(color, inner);
}, new Map());

module.exports = {
  inputs: {
    part1: new Map(bagMap),
    part2: new Map(bagMap),
  },

  expectedOutputs: {
    part1: 238,
    part2: 82930,
  },
};
