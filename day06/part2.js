module.exports = groups => {
  const answers = groups.map(group => {
    const uniqueChars = [...new Set(group.combinedAnswer)];

    const allAnswered = uniqueChars.filter(c => {
      const countInStr = [...group.combinedAnswer].filter(x => x === c).length;
      return countInStr === group.size;
    });

    return allAnswered.length;
  });

  const sum = answers.reduce((a, b) => a + b);
  return sum;
};
