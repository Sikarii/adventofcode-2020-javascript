const { getInputLines } = require("../helpers");

const groups = getInputLines(__dirname, "\r\n\r\n");

const groupAnswers = groups.map(group => {
  const answers = group.split("\r\n");
  const answersCombined = answers.join("");
  return {
    size: answers.length,
    combinedAnswer: answersCombined,
  };
});

module.exports = {
  inputs: {
    part1: [...groupAnswers],
    part2: [...groupAnswers],
  },

  expectedOutputs: {
    part1: 6457,
    part2: 3260,
  },
};
