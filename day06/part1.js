module.exports = groups => {
  const answers = groups.map(group => {
    const uniqueChars = [...new Set(group.combinedAnswer)];
    return uniqueChars.length;
  });

  const sum = answers.reduce((a, b) => a + b);
  return sum;
};
