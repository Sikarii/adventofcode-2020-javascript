const { getInputLines } = require("../helpers");

const lines = getInputLines(__dirname);
const actions = lines.map(l => {
  const [direction, ...rest] = l;
  const amount = parseInt(rest.join(""));

  return {
    direction,
    amount,
  };
});

module.exports = {
  inputs: {
    part1: [...actions],
    part2: [...actions],
  },

  expectedOutputs: {
    part1: 2280,
    part2: 38693,
  },

  shared: {},
};
