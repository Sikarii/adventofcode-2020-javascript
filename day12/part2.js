module.exports = actions => {
  // E, N
  let shipPos = [0, 0];
  let waypointPos = [10, 1];

  const moves = {
    // Directions
    N: amt => {
      waypointPos[1] += amt;
    },

    E: amt => {
      waypointPos[0] += amt;
    },

    S: amt => {
      waypointPos[1] -= amt;
    },

    W: amt => {
      waypointPos[0] -= amt;
    },

    L: amt => {
      const rotations = amt / 90;
      for (let i = 0; i < rotations; i++) {
        const oldPos = [...waypointPos];
        waypointPos[0] = -oldPos[1];
        waypointPos[1] = oldPos[0];
      }
    },

    R: amt => {
      const rotations = amt / 90;
      for (let i = 0; i < rotations; i++) {
        const oldPos = [...waypointPos];
        waypointPos[0] = oldPos[1];
        waypointPos[1] = -oldPos[0];
      }
    },

    F: amt => {
      shipPos[0] += waypointPos[0] * amt;
      shipPos[1] += waypointPos[1] * amt;
    },
  };

  for (const action of actions) {
    const move = moves[action.direction];
    move(action.amount);
  }

  return Math.abs(shipPos[0]) + Math.abs(shipPos[1]);
};
