module.exports = actions => {
  let degrees = 0;

  // E, N
  let position = [0, 0];

  const directions = {
    "-1": "N",
    "-2": "W",
    "-3": "S",
    0: "E",
    1: "S",
    2: "W",
    3: "N",
  };

  const moves = {
    // Angle
    L: amt => {
      degrees -= amt;
    },

    R: amt => {
      degrees += amt;
    },

    // Directions
    N: amt => {
      position[1] += amt;
    },

    E: amt => {
      position[0] += amt;
    },

    S: amt => {
      position[1] -= amt;
    },

    W: amt => {
      position[0] -= amt;
    },

    F: amt => {
      const dir = directions[(degrees % 360) / 90];

      if (dir === "N") {
        position[1] += amt;
      } else if (dir === "E") {
        position[0] += amt;
      } else if (dir === "S") {
        position[1] -= amt;
      } else if (dir === "W") {
        position[0] -= amt;
      }
    },
  };

  for (const action of actions) {
    const move = moves[action.direction];
    move(action.amount);
  }

  return Math.abs(position[0]) + Math.abs(position[1]);
};
